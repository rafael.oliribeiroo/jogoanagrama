import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import SplashScreen from '../Pages/SplashScreen/index';
import Home from '../Pages/Home/index';

export default () => {

    return(
       
        <Switch>

            <Route exact path="/" component={SplashScreen} />             
            <Route path="/Home" component={Home} />
                        
        </Switch>
      

    );
};
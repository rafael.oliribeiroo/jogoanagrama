import React, { useState } from 'react';
import './styles.css';

import CircleAnimation from '../../components/CircleAnimation';
import AnagramInput from '../../components/AnagramInput/index';

import exemploAFD from '../../assets/exemploAFD.png'
import exemploAFD1 from '../../assets/exemploAFD1.png'


const Home = (props) => {     
    
    const [input, setInput] = useState('');
    const [inputSeparated, setInputSeparated] = useState('');
    const [separed, setsepared] = useState('');
    
    // async function getSeparatedInputWord (input){          //verificar o caso que não vem nada 
    //     let separatedWord = input.split('');   
    //     console.log(separatedWord);
    //     return separatedWord;
    // }

    // async function getInputWord ({input}) { // verificar o caso que não vem nada.
    //     console.log(input);
    //     const word = {input}.toString();
    //     console.log(word);
    //     return word;   
    // }
    
    const separatedInputWord = input.toString().split('');
    //  const inputSeparatedWord = getSeparatedInputWord();
    // const inputWord = getInputWord();
    
    return(

        <>           
            <h1>Anagrama Game + AFD</h1> 
                        
            <div class="AlignComponents">
                <CircleAnimation getInput={input} separatedInput={separatedInputWord}/>
                <AnagramInput 
                getInput={input => setInput(input)} 
                getSeparatedInput={input => setInputSeparated(input)} 
                />              
            </div>

            <div class="alignContainerRules">

                <div class="anotherGameRulesContainer">
                    <div class="gameRulesTitle">Regras e Usabilidade</div>
                    <div class="contentRules">
                        <p class="rule"> 1 - A palavra " M E L A N C I A " é o anagrama sugerido. Para jogar, basta inserir possíveis palavras (anagramas) que possuam as letras que a palavra em destaque sugerida possui. A palavra inserida será analisada pelo simulador AFD e informará se a mesma é aprovada ou não. Exemplo de anagrama: " A M O R " - ' roma '.</p>
                        <p class="rule"> 2 - Após a análise da palavra, você poderá realizar uma nova entrada de palavra para o simulador e assim sucessivamente até que todas as palavras sejam encontradas. Quando isso acontecer, ele emitirá uma mensagem informando que todas as palavras possíveis processadas pelo simulador foram geradas. </p>
                        <p class="rule"> 3 - As palavras inseridas para verificação devem estar em caixa-baixa (lowercase) e, também, não serão consideradas as acentuações. Ex:"palavra"</p>
                        <p class="rule"> 4 - Após a inserção do anagrama para validação, o simulador AFD analisará a palavra e, se a mesma terminar em um dos seus possíveis estados finais após a leitura inteira da palavra, irá sinalizar se a mesma foi aprovada e pertence ao alfabeto deste autômato.</p>
                        <p class="rule"> 5 - A elaboração deste simulador é apenas simbólica, as regras e expressões regulares que definem os padrões de aceitação do mesmo não serão trabalhados neste simulador. Para saber mais sobre este tópico, basta acessar o link disponível no botão abaixo.</p>
                        <a href="http://www.ybadoo.com.br/tutoriais/lfa/06/LFA06.pdf" target="_blank" ><button class="button">Linguagens regulares</button></a>
                    </div>
                </div>  

                <div class="gameRulesContainer">
                    <div class="gameRulesTitle">Conceitos Aplicados e AFD</div>
                    <div class="contentRules">
                        <p class="rule">Uma máquina de estados finita determinística ou autômato finito determinístico (AFD) é uma máquina de estados finitos onde, dado uma configuração e um símbolo da cadeia de entrada existe somente um estado para o qual a máquina pode transitar.</p>
                        <img src={exemploAFD} class="imageRule" alt="exemploAFD"/>
                        <div class="rule">Um autômato finito tem um conjunto de estados, alguns dos quais são denominados estados finais. À medida que caracteres da string de entrada são lidos, o controle da máquina passa de um estado a outro, segundo um conjunto de regras de transição especificadas para o autômato.</div>
                        <img src={exemploAFD1} class="imageRule" alt="exemploAFD1" />
                    </div>
                </div> 
            </div> 

        </>   
    );
};

export default Home;
import React from 'react';
import './styles.css';

import Step1 from '../../components/Step1'


const SplashScreen = () => {
    return(
        <>           
            
            <div class="alignCard">  
                <Step1 />                           
            </div>
        </>        
    );
};

export default SplashScreen;
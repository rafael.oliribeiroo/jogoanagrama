import React, { useState, useEffect } from 'react';
import './styles.css';

const { AnagramWord } = require('../../Shared/Anagrams');

export default function CircleAnimation(props)  {  
    
    const [ situation, setSituation ] = useState('');

    useEffect(() => {
        const loadAll = async () => {
            // ShowAnimationAFD( props.separatedInput, props.getInput);    
            ValidaWord(props.getInput);
        }

        loadAll();
    }, [props.getInput]);

        //  const ShowAnimationAFD = async () => {
    
    //     const wordInputSeparated = props.separatedInput;
    //     const wordInput = await props.getInput;
    //     console.log(wordInputSeparated[5]);
        
    //     const b = document.querySelector('.circle');     
    //     const c = document.querySelector('.circleContent');
    //     const d = document.querySelector('.processWord');
    //     const e = document.querySelector('.NotApproved');
    //     console.log(d.textContent); // imprime o valor da div 


        const ValidaWord = async () => {

            const b = document.querySelector('.circle');   
            
            if(props.getInput === ''){
                b.classList.remove('NotApproved'); 
                b.classList.remove('Approved'); 

            }else if(AnagramWord.includes(props.getInput)){

                b.classList.remove('NotApproved'); 
                b.classList.add('Approved');                 
                setSituation('Approved')

            }else {                   
                b.classList.add('NotApproved');
                b.classList.remove('Approved');                              
                setSituation('NotApproved')
            };
        };      

    const createNotApprovedAlert = () => {     
        
        //adicionando alerta de mensagem aprovada
        
        // const newElement = document.createElement("div");
        // const newElementText = document.createTextNode("Sua palavra não foi aceita!");
        // newElement.appendChild(newElementText);

        // const newElementPosition = document.querySelector('.alertWord');
        // newElementPosition.appendChild(newElement);
        // const newElement = document.querySelector('.circle');
        // newElement.classList.remove('NotApproved');
        // newElement.classList.add('NotApproved');

        return (
            <div class="alertWord">
                <div class="NotApproved">Sua palavra não foi aceita!</div>
            </div>
        );
        
    }

    const createApprovedAlert = () => {

        //adicionando alerta de mensagem aprovada

        // const newElement = document.createElement("div");
        // const newElementText = document.createTextNode("Sua palavra foi aceita!");
        // newElement.appendChild(newElementText);

        // const newElementPosition = document.querySelector('.alertWord');
        // newElementPosition.appendChild(newElement);
        // newElement.classList.remove('NotApproved');
        // newElement.classList.add('Approved');

        return (
            <div class="alertWord">
                <div class="NotApproved">Sua palavra foi aceita!</div>
            </div>
        );
    }
    
       
    return (
        <> 

        <div class="containerCircle">
            <div class="containerTitle1">
                <p class="mainTitle">Simulador AFD</p>
                <p class="processWord">{props.getInput}</p>
            </div>

            <div class="circle">
                <div class="contentCircle">
                    <p class="circleContent"></p>
                </div>
            </div>  
           

            {
                situation === 'Approved' && 
                createApprovedAlert()
            }

            {
                situation === 'NotApproved' && 
                createNotApprovedAlert()
            }


        </div>   

        </>
    );
};
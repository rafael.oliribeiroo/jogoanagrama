import React, { useState } from 'react';
import './styles.css';

const { AnagramWord } = require('../../Shared/Anagrams');

export default function AnagramInput(props)  {  
  
  const [InputList, setInputList] = useState([]);
  const [inputWord, setInputWord] = useState('');

  function handleChange(event) {
    setInputWord(event.target.value);    
  }

  function handleClick() {
    setInputList([inputWord]);
    setInputWord('');
  }

  const someFunctions = () => { 
    handleClick();
    props.getSeparatedInput([inputWord]);
    props.getInput(inputWord);
    verifyWord(inputWord, AnagramWord);
  }

  const verifyWord = (inputWord, AnagramWord) => {  
    AnagramWord.includes(inputWord) ? alert("Encontrou") : alert("Não encontrou");
  };
  
  
  
  return (

      <div class="CurrentComponent">

        <div class="containerAnagram">
            <div class="containerTitle">
                <div class="title1"> M E L A N C I A</div>                   
            </div>           
            
            {/* Como acessar o valor pego no input */}    
            {/* <p>{InputList.map((word) => (    
              //props.pegaNome({name})          
              [word]              
            ))}</p> */}
            
            <div class="containerInput">
                <input class="input"  value={inputWord} placeholder="insira uma palavra" name="entry" onChange={handleChange} type="text" /> 
                <button class="buttonInput" type="button" onClick={() => someFunctions()}>Analisar</button>
            </div>  
        </div> 
        
      </div>

  );
};

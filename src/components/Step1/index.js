import React from 'react';
import './styles.css';
import { Link } from 'react-router-dom'

export default function Step1()  {    

    return (
          
        <div class="cardGeneral">
            <div class="cardTitle">
                <p>Olá, bem-vindes!</p>
            </div>
            <div class="cardContent">
                <p>Esta simulação de jogo anagrama foi desenvolvida durante a realização da disciplina de Linguagens Formais, ministrada pelo professor Luís Menasché, do curso de Ciência da Computação da UFRJ, durante o período remoto de 2021.1.
                    A ideia principal é trazer a aplicação do jogo de anagrama e correlacioná-la com o conteúdo de autômatos finitos determiniístico (AFD) apresentado durante os estudos da disciplina.
                </p>
            </div>   
            <div class="alignButtonCard">
                <Link to='/Home'class="buttonCard">Começar!</Link>             
            </div>
        </div>                
        
    );
};